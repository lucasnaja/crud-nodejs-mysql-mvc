# CRUD - NodeJS and MySQL

## Author: Lucas Bittencourt

### Used Dependencies
- express
- sequelize
- mysql2
- cors

### Used Dev Dependencies
- nodemon
- @types/express
- @types/sequelize
- @types/cors

### How to
- Test
    - `npm install`
    - Docker commands
        - `docker image pull mysql:latest`
        - `docker container run -p 3306:3306 --name mysql -e MYSQL_ROOT_PASSWORD=secret -e MYSQL_ROOT_HOST=% -d mysql:latest --default-authentication-plugin=mysql_native_password`
    - `npm start`
    - Insomnia or CURL


- CREATE
```sh
curl --request POST \
  --url http://localhost:3000/api/users \
  --header 'content-type: application/json' \
  --data '{
	"user_firstName": "Lucas",
	"user_lastName": "Bittencourt",
	"user_email": "lucasnaja0@gmail.com",
	"user_birth": "1999/07/30"
}'
```
- GET ALL DATA
```sh
curl --request GET \
  --url http://localhost:3000/api/users
```

- GET SOME DATA
```sh
curl --request GET \
  --url http://localhost:3000/api/users/1
```

- UPDATE ALL DATA
```sh
curl --request PUT \
  --url http://localhost:3000/api/users/1 \
  --header 'content-type: application/json' \
  --data '{
	"user_firstName": "João",
	"user_lastName": "Neto",
	"user_email": "lucasnaja0@gmail.com",
	"user_birth": "1999/07/30"
}'
```

- UPDATE SOME DATA
```sh
curl --request PATCH \
  --url http://localhost:3000/api/users/1 \
  --header 'content-type: application/json' \
  --data '{
	"user_email": "jaoneto@hotmail.com",
	"user_birth": "2002/01/13"
}'
```

- DELETE
```sh
curl --request DELETE \
  --url http://localhost:3000/api/users/1
```