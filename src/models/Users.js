const { STRING, INTEGER, DATE } = require('sequelize')

module.exports = sqlize => {
    const Users = sqlize.define(
        'users', {
            user_id: {
                type: INTEGER,
                required: true,
                primaryKey: true,
                autoIncrement: true
            },
            user_firstName: {
                type: STRING,
                allowNull: false
            },
            user_lastName: {
                type: STRING,
                allowNull: false
            },
            user_email: STRING,
            user_birth: {
                type: DATE,
                allowNull: false
            }
        }, {
            tableName: 'users',
            freezeTableName: false,
            timestamps: false
        }
    )

    Users.sync()

    return Users
}
