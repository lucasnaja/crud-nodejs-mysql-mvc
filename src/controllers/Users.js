module.exports = sqlize => {
    const UsersModel = require('../models/Users')(sqlize)

    return {
        async createUser(req, res) {
            const user = await UsersModel.create(req.body)

            return res.status(201).json(user)
        },

        async getAllUsers(req, res) {
            const users = await UsersModel.findAll({ raw: true })

            return res.status(200).json(users)
        },

        async getSomeUser(req, res) {
            const user = await UsersModel.findOne({ where: { user_id: req.params.user_id } })

            return res.status(200).json(user ? user : { message: 'No data found.' })
        },

        async updateAllUserData(req, res) {
            const [success] = await UsersModel.update(req.body, { where: { user_id: req.params.user_id } })
            const message = success ? 'Successfully updated data.' : 'Unsuccessfully update data.'

            return res.status(200).json({ success, message })
        },

        async updateSomeUserData(req, res) {
            const [success] = await UsersModel.update(req.body, { where: { user_id: req.params.user_id } })
            const message = success ? 'Successfully updated data.' : 'Unsuccessfully update data.'

            return res.status(200).json({ success, message })
        },

        async deleteUser(req, res) {
            const success = await UsersModel.destroy({ where: { user_id: req.params.user_id } })
            const message = success ? 'Successfully deleted data.' : 'Unsuccessfully delete data.'

            return res.status(200).json({ success, message })
        }
    }
}
