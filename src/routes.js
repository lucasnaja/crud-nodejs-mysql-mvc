const routes = require('express').Router()
const Sequelize = require('sequelize')

const sqlize = new Sequelize('nodejs', 'root', 'secret', {
    host: 'localhost',
    dialect: 'mysql'
})

const UsersController = require('./controllers/Users')(sqlize)

routes.post('/users', UsersController.createUser)
routes.get('/users', UsersController.getAllUsers)
routes.get('/users/:user_id', UsersController.getSomeUser)
routes.put('/users/:user_id', UsersController.updateAllUserData)
routes.patch('/users/:user_id', UsersController.updateSomeUserData)
routes.delete('/users/:user_id', UsersController.deleteUser)

module.exports = routes
